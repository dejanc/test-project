# DEPRECATED CLASS
class RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
      build_resource(sign_up_params)
      if resource.save
        if resource.active_for_authentication?
          sign_in(resource_name, resource)
          redirect_to root_url
        else
          set_flash_message :notice, :inactive_signed_up, :reason => resource.inactive_message.to_s
          expire_session_data_after_sign_in!
          redirect_to after_inactive_sign_up_path_for(resource)
        end
      else
        clean_up_passwords(resource)
        render :new
      end

  end

end
