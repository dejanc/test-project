class InvitationsController < ApplicationController

  before_filter :authenticate_admin!

  def new
    @invitation = Invitation.new

    respond_to do |format|
      format.html
      format.json { render json: @invitation }
    end
  end

  def create
    emails = params[:invitation][:invited_email] ? params[:invitation][:invited_email].split(',').map(&:strip) : []
    if emails.any? && (params[:invitation][:trial_ids] && params[:invitation][:trial_ids].count == 1)
      emails.each do |email|
        invitation = Invitation.new(params[:invitation])
        invitation.invited_email = email
        invitation.admin = current_admin
        if invitation.save
          flash[:success] = "#{email} invited with key #{invitation.invited_key}"
        else
          flash[:error] = "#{email} wasn't invited"
        end
      end
    else
      flash[:error] = "Select one resource"
    end
    respond_to do |format|
      format.html { redirect_to action: "new" }
    end
  end

  def create_existing
    user_id = params[:invitation][:user_id]
    invitation = Invitation.new(params[:invitation])
    invitation.user_id = user_id
    invitation.admin = current_admin

    respond_to do |format|
      if invitation.save
        flash[:success] = "Resource assigned"
        format.html { redirect_to action: "edit_permissions", user_id: user_id  }
      else
        flash[:error] = "Couldn't assign resource"
        format.html { redirect_to action: "edit_permissions", user_id: user_id  }
      end
    end
  end

  def edit_permissions
    @invitation = Invitation.new
    @user = User.find params[:user_id]
  end

end