class SelfMailer < ActionMailer::Base

  # got Invitation object
  def invitation(object)
    @invitation = object
    mail :to => @invitation.invited_email, :subject => "Your invitation", :from => "hi@test.com"
  end

  def new_permissions(object)
    # email user notifying him about new permissions
  end

end