class User < ActiveRecord::Base

  require 'digest/md5'

  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me

  has_many :invitations
  has_many :trials, :through => :invitations

  validate :good_key, :on => :create
  after_create :tie_with_invitation

  # param can be id or Trial obj
  def can_view_trial?(trial)
    if trial.is_a?(Trial)
      obj = trial
    else
      obj = Trial.find trial
    end
    obj && self.trials.include?(obj)
  end

  private

  # NOTE: user has to register with the email he was invited with because of the code below
  # pass in additional param like "original_email" and check that if you want to be able to use different email for register
  def good_key
    invitation = Invitation.where("invited_email = ?", self.email).first
    if invitation.nil? || (invitation.invited_key != Digest::MD5.hexdigest("salt#{self.email}")) # TODO: move salt to constant
      errors.add(:invited_key, 'is wrong')
    end
  end

  def tie_with_invitation
    invitation = Invitation.where("invited_email = ?", self.email).first
    invitation.update_attribute(:user_id, self.id) if invitation
  end

end
