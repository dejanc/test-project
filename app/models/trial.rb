class Trial < ActiveRecord::Base
  attr_accessible :title

  has_and_belongs_to_many :invitations

  validates_presence_of :title

end
