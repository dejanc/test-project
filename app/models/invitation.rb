class Invitation < ActiveRecord::Base

  require 'digest/md5'

  attr_accessible :invited_email, :trial_ids, :user_id

  scope :not_yet_registered, where(:user_id => nil)

  before_save :generate_key
  after_create :email_user
  before_destroy :notify_user_removed

  belongs_to :admin
  belongs_to :user

  has_and_belongs_to_many :trials
  accepts_nested_attributes_for :trials

  validates_presence_of :admin_id
  validate :invited_email_or_id
  validates_format_of :invited_email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :allow_nil => true

  private

  def generate_key
    # TODO: move salt to constant later
    # only for non existing users, don't need generate it for invitation to new resources
    self.invited_key = Digest::MD5.hexdigest("salt#{self.invited_email}") if self.user_id.nil?
  end

  def email_user
    # Sending invitation for existing user for changing it's permissions
    if self.user_id
      SelfMailer.new_permissions(self).deliver
    else
      SelfMailer.invitation(self).deliver
    end
  end

  def invited_email_or_id
    if invited_email.blank? && user_id.blank?
      errors.add(:base, "Need to add user or email")
    end
  end

  def notify_user_removed
    # Send email notifying user about removed permissions
  end

end
