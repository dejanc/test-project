class CreateInvitationsAndTrials < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.integer :admin_id, :null => false
      t.integer :user_id
      t.string :invited_email
      t.string :invited_key
      t.timestamps
    end

    create_table :trials do |t|
      t.string :title, :null => false, :default => ""
      t.timestamps
    end

    create_table :invitations_trials, :id => false do |t|
      t.references :trial
      t.references :invitation
    end

    add_index :invitations_trials, [:trial_id, :invitation_id]
    add_index :invitations_trials, :invitation_id

  end
end
